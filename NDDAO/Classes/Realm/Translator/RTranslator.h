//
//  RTranslator.h
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 07.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol RTranslator <NSObject>

- (Class)entityClass;

- (Class)entryClass;

- (id)entityWithEntry:(id)entry;

- (id)entryWithEntity:(id)entity;

@end
