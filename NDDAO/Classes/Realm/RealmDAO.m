//
//  RealmDAO.m
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 07.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import "RealmDAO.h"
#import <Realm/Realm.h>
#import <Realm/RLMObject.h>


@interface RealmDAO ()

@property (nonatomic, strong) id<RTranslator> translator;
@property (nonatomic, strong) RLMRealm *realm;

@end


@implementation RealmDAO

- (instancetype)initWithTranslator:(id<RTranslator>)translator
                     encryptionKey:(NSString *)encryptionKey
{
    if (self = [super init])
    {
        [self createEncryptedRealmWithKey:encryptionKey];
        
        _translator = translator;
        NSLog(@"Realm path: %@", _realm.path);
    }
    return self;
}


#pragma mark - Шифрование

- (void)createEncryptedRealmWithKey:(NSString *)encryptionKey
{
    NSData *keyData = [encryptionKey dataUsingEncoding:NSUTF8StringEncoding];
    
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.readOnly = NO;
    if (keyData)
    {
        config.encryptionKey = keyData;
    }
    _realm = [RLMRealm realmWithConfiguration:config error:nil];
}


#pragma mark - Действия

- (BOOL)persist:(id)entity
{
    [self.realm beginWriteTransaction];
    
    id object = [self.translator entryWithEntity:entity];
    [self.realm addOrUpdateObject:object];
    
    return [self.realm commitWriteTransaction:nil];
}

- (BOOL)persistAll:(NSArray *)entities
{
    BOOL saved = YES;
    for (id entity in entities)
    {
        saved = [self persist:entity];
        if (!saved)
        {
            break;
        }
    }
    
    return saved;
}

- (BOOL)erase:(id)itemId
{
    return NO;
}

- (BOOL)eraseAll
{
    return NO;
}

- (id)read:(id)itemId
{
    return nil;
}

- (NSArray *)readAll
{
    NSMutableArray *entities = [@[] mutableCopy];
    RLMResults *fetchedObjects = [[self.translator entryClass] allObjectsInRealm:self.realm];
    
    for (id entry in fetchedObjects)
    {
        id entity = [self.translator entityWithEntry:entry];
        [entities addObject:entity];
    }
    
    return [NSArray arrayWithArray:entities];
}

@end
