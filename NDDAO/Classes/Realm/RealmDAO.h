//
//  RealmDAO.h
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 07.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DAO.h"
#import "RTranslator.h"


@interface RealmDAO : NSObject <DAO>

- (instancetype)initWithTranslator:(id<RTranslator>)translator
                     encryptionKey:(NSString *)encryptionKey;

- (instancetype)init __unavailable;

@end
