//
//  CoreDataDAO.m
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 07.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import "CoreDataDAO.h"
#import <CoreData/CoreData.h>


@interface CoreDataDAO ()

@property (nonatomic, strong) id<CDTranslator> translator;
@property (nonatomic, copy) NSString *dbName;
@property (nonatomic, copy) NSString *modelName;

@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSManagedObjectContext *context;

@end


@implementation CoreDataDAO

- (instancetype)initWithTranslator:(id<CDTranslator>)translator
                         modelName:(NSString *)modelName
                            dbName:(NSString *)dbName
                  storeCoordinator:(NSPersistentStoreCoordinator *)coordinator
{
    if (self = [super init])
    {
        _translator = translator;
        _dbName = [dbName copy];
        _modelName = [modelName copy];
        
        _persistentStoreCoordinator = coordinator;
        _context = [[NSManagedObjectContext alloc]
                        initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_context setPersistentStoreCoordinator:_persistentStoreCoordinator];
    }
    return self;
}

- (BOOL)persist:(id)entity
{
    [self.translator entryFromEntity:entity context:self.context];
    
    return [self.context save:nil];
}

- (BOOL)persistAll:(NSArray *)entities
{
    BOOL saved = YES;
    for (id entity in entities)
    {
        saved = [self persist:entity];
        if (!saved)
        {
            break;
        }
    }
    return saved;
}

- (BOOL)erase:(id)itemId
{
    return NO;
}

- (BOOL)eraseAll
{
    NSArray *objects = [self fetchObjects];
    for (NSManagedObject *object in objects)
    {
        [self.context deleteObject:object];
    }
    
    return [self.context save:nil];;
}

- (id)read:(id)itemId
{
    return nil;
}

- (NSArray *)readAll
{
    NSMutableArray *entities = [@[] mutableCopy];
    NSArray *fetchedObjects = [self fetchObjects];
    for (id entry in fetchedObjects)
    {
        id entity = [self.translator entityWithEntry:entry];
        [entities addObject:entity];
    }
    
    return [NSArray arrayWithArray:entities];
}

- (NSArray *)fetchObjects
{
    NSString *className = NSStringFromClass([self.translator entryClass]);
    NSEntityDescription *entity = [NSEntityDescription entityForName:className
                                              inManagedObjectContext:self.context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.includesPropertyValues = YES;
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest
                                                          error:nil];
    return fetchedObjects;
}

@end
