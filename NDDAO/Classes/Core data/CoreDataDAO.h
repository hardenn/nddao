//
//  CoreDataDAO.h
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 07.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DAO.h"
#import "CDTranslator.h"


@interface CoreDataDAO : NSObject <DAO>

- (instancetype)initWithTranslator:(id<CDTranslator>)translator
                         modelName:(NSString *)modelName
                            dbName:(NSString *)dbName
                  storeCoordinator:(NSPersistentStoreCoordinator *)coordinator;

- (instancetype)init __unavailable;

@end
