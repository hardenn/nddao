//
//  CDTranslator.h
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 07.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@protocol CDTranslator <NSObject>

- (Class)entityClass;

- (Class)entryClass;

- (id)entityWithEntry:(id)entry;

- (id)entryFromEntity:(id)entity
              context:(NSManagedObjectContext *)context;

@end
