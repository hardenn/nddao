//
//  DAOProtocol.h
//  AlfaStrakhovanie
//
//  Created by ndmitrieva on 05.02.16.
//  Copyright © 2016 n.dmitrieva. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol DAO <NSObject>

- (BOOL)persist:(id)entity;

- (BOOL)persistAll:(NSArray *)entities;

- (NSArray *)readAll;

- (id)read:(id)itemId __unavailable;

- (BOOL)erase:(id)itemId __unavailable;

- (BOOL)eraseAll;

@end
