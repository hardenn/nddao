Pod::Spec.new do |s|
  s.name             = "NDDAO"
  s.version          = "0.1.0"
  s.summary          = "Library for using CoreData and Realm."
  s.homepage         = "https://bitbucket.org/hardenn/nddao"
  s.license          = 'Code is MIT.'
  s.author           = { "Nino Dmitrieva" => "firedru@gmail.com" }
  s.source           = { :git => "https://hardenn@bitbucket.org/hardenn/nddao.git", :tag => s.version }
  s.social_media_url = 'https://ru.linkedin.com/in/ninadmitrieva'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.frameworks = 'CoreData'
  s.source_files = "NDDAO/Classes/**/*.{h,m}"
  s.dependency 'Realm', '~> 0.97'

end